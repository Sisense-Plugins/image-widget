mod.directive('imagewidget', [

    function ($timeout, $dom) {

        return {
            priority: 0,
            replace: false,
            templateUrl: "/plugins/imageWidget/imageWidget.html",
            transclude: false,
            restrict: 'E',
            link: function ($scope, lmnt, attrs) {
				// we need to wait until image itself is finished, after wht domready event should be triggered on widget
				// this is critical as some other plugins can relay on widget domready event, and thus it will cause incorrect work if image widget is used
				$("img", lmnt[0]).one("load", function() {
					$scope.widget.trigger('domready');
				}).each(function() {
					if(this.complete) $(this).load();
				});
            }
        }
    }]);

