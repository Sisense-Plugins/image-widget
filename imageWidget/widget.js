prism.registerWidget("imageWidget", {
    name: "imageWidget",
    family: "images",
    title: "imageWidget",
    iconSmall: "/plugins/imageWidget/imageWidget-24.png",
    styleEditorTemplate: "/plugins/imageWidget/styler.html",
    hideNoResults: true,
    style: {
        src: '',
        align: 'left',
        transform: 'original',
        width: 0.12,
        showMinMax: false,
		link: ''
    },
    directive: {
        desktop: "imagewidget"
    },
    data: {
        selection: [],
        defaultQueryResult: {},
        panels: [
            {
                name : 'URL',
                type : 'visible',
                metadata : {

                    types : ['dimensions'],
                    maxitems : 1
                }
            }, 
            {
                name: 'filters',
                type: 'filters',
                metadata: {

                    types: ['dimensions'],
                    maxitems: -1
                }
            }
        ],

        allocatePanel : function (widget, metadataItem) {
            if (!prism.$jaql.isMeasure(metadataItem) && widget.metadata.panel("URL").items.length < 1) {
                return "URL";
            }
        },

        // ranks the compatibility of the given metadata items with the widget
        rankMetadata : function (items, type, subtype) {
            return -1;
        },

        // populates the metadata items to the widget
        populateMetadata : function (widget, items) {
            
            var a = prism.$jaql.analyze(items);

            // allocating dimensions
            widget.metadata.panel("URL").push(a.dimensions);
            widget.metadata.panel("filters").push(a.filters);
        },

        // builds a jaql query from the given widget
        buildQuery : function (widget) {

            // building jaql query object from widget metadata
            var query = {
                datasource : widget.datasource,
                metadata : [],
                count: 1,
                offset: 0
            };

            // pushing items
            widget.metadata.panel("URL").items.forEach(function (item) {

                query.metadata.push(item);
            });
            
            // filters
            widget.metadata.panel('filters').items.forEach(function (item) {

                item = $$.object.clone(item, true);
                item.panel = "scope";

                query.metadata.push(item);
            });


            return query;
        },

        // prepares the widget-specific query result from the given result data-table
        processResult : function (widget, queryResult) {

            //  Look for the first record returned
            var newUrl = queryResult.$$rows[0][0];

            //  Were there any results?
            if (newUrl) {

                //  If so, set the url property
                widget.style.src = newUrl.text;
                
                //  Look for this widget
                var thisWidget = $('widget[widgetid=' + widget.oid + ']');

                //  Look for this iframe
                var image = $('.sisenseImage', thisWidget);

                //  Change the source
                image.attr('src',newUrl.text);
            }
        }
    },
    options: {
        dashboardFiltersMode: "slice",
        selector: false,
        title: false
    },
    sizing: {
        minHeight: 128, //header
        maxHeight: 2048,
        minWidth: 128,
        maxWidth: 2048,
        height: 320,
        defaultWidth: 512
    }
});
