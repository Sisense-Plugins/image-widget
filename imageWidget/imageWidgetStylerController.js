mod.controller('imageWidgetStylerController', ['$scope',
    function ($scope) {


        /**
         * variables
         */


        /**
         * watches
         */
        $scope.$watch('widget', function (val) {

            $scope.model = $$get($scope, 'widget.style');
        });

        /**
         * public methods
         */
		 
        $scope.onChange = function ($event) {
            _.defer(function () {
                $scope.$root.widget.redraw();
            });
        };

        //get original widget alignment
        $scope.alignImage = $$get($scope.$root.widget, 'style.align') || 'left';

        //get original widget transform
        $scope.transformImage = $$get($scope.$root.widget, 'style.transform') || 'original';

        $scope.changeAlign = function (align) {

            //apply changes
            $scope.alignImage = align;

            //save changes to widget
            $$set($scope.$root.widget, 'style.align', $scope.alignImage);

            //redraw widget
            _.defer(function () {
                $scope.$root.widget.redraw();
            });

        };

        $scope.changeTransform = function (transform) {

            //apply changes
            $scope.transformImage = transform;

            //save changes to widget
            $$set($scope.$root.widget, 'style.transform', $scope.transformImage);
            //redraw widget
            _.defer(function () {
                $scope.$root.widget.redraw();
            });

        };


    }
]);